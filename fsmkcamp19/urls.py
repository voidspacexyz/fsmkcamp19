"""fsmkcamp19 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from webbrowser import register

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path

from fsmkcamp19.views import home, register, view_mail, me_in_camp, updatead
from fsmkcamp19.views import check_in, arrived, feedback, hacktivist_camp_view
from fsmkcamp19.views import dashboard, view_feedback


urlpatterns = [
    path('',home),
    path("register/",register),
    path("view_mail/",view_mail),
    path("me_in_camp/",me_in_camp),
    path("checkin/",check_in),
    path("arrived/",arrived),
    path("updatead/",updatead),
    path("feedback/",feedback),
    path("view_feedback/",feedback),
    path("ratatata/",view_feedback),
    path("hacktivist_camp/",hacktivist_camp_view),
    path('nimda/', admin.site.urls),
    path("dashboard/",dashboard),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)