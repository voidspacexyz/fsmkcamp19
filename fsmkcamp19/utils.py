
from django.core.mail import EmailMessage, send_mail
from django.template.loader import render_to_string
from django.conf import settings

def send_html_email(to_list, context, subject="FSMKCamps'20 Registration", template_name="register_success.html",  sender=settings.DEFAULT_FROM_EMAIL):
    msg_html = render_to_string(template_name, context)
    msg = EmailMessage(subject=subject, body=msg_html, from_email="camp@fsmk.org", bcc=to_list)
    msg.content_subtype = "html"  # Main content is now text/html
    return msg.send()