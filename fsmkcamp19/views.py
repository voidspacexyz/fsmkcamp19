
from django.conf import settings
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.conf import settings
from django.db.models import Count
from django.contrib.admin.views.decorators import staff_member_required
from django.db.models import Q


from fsmkcamp19.admin import Volunteers
from fsmkcamp19.forms import ArrivalDepartureForm, FeedbackForm, \
    RegistrationForm
from fsmkcamp19.models import Colleges, FAQ, Registration, Speakers, Track, Testimonial, Feedback
from fsmkcamp19.utils import send_html_email


def home(request):
    total_seats_remaining = 0#settings.FULL_COUNT - Registration.objects.filter(is_confirmed=True).count()
    tracks = Track.objects.all()
    speakers = Speakers.objects.all()
    colleges = Colleges.objects.all().order_by("college")
    reg_form = RegistrationForm()
    faqs = FAQ.objects.all()
    testimonials = Testimonial.objects.all()
    return render(request, "index.html", {"total_seats_remaining": total_seats_remaining,"tracks":tracks,"speakers":speakers, "colleges":colleges,"reg_form":reg_form,'faqs':faqs,"testimonials":testimonials})

def feedback(request):
    if request.POST:
        form = FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
        return HttpResponseRedirect("/feedback/") 
    else:
        form = FeedbackForm()
        return render(request,"feedback.html",{"form":form})

@staff_member_required
def view_feedback(request):
    feedbacks = Feedback.objects.all().order_by('-created_date')
    return render(request, "view_feedback.html",{"feedbacks":feedbacks})


def hacktivist_camp_view(request):
    if request.POST:
        email = request.POST.get("email")
        contact = request.POST.get("contact")
        choice = request.POST.get("choice")
        participant = Registration.objects.filter(email=email,contact=contact,is_confirmed=True).first()
        if not participant:
            participant = Volunteers.objects.filter(email=email,contact=contact).first()
        
        if participant and choice:
            participant.hacktivist_camp = choice 
            participant.save()
            return render(request,"hacktivist_camp.html",{"success":"Updated the information"})
        
        if not choice:
            return render(request,"hacktivist_camp.html",{"participant":participant})
        else:
            return render(request,"hacktivist_camp.html",{"found":False})

    else:
        return render(request,"hacktivist_camp.html",{})

def me_in_camp(request):
    if request.POST:
        email = request.POST.get("email")
        contact = request.POST.get("contact")
        participant = Registration.objects.filter(email=email,contact=contact,is_confirmed=True).first()
        if participant:
            adform = ArrivalDepartureForm({"id":participant.id,"arrival":participant.arrival,"departure":participant.departure})
            return render(request,"foundya.html",{"adform":adform,"id":participant.id})
        else:
            return render(request,"me_in_camp.html",{"found":False})

    else:
        return render(request,"me_in_camp.html",{"found":True})

@require_http_methods(["POST"])
def arrived(request):
    pid = request.POST.get("participant_id")
    ptype = request.POST.get("ptype")
    if ptype=="participant":
        participant = Registration.objects.get(id=pid)
    else:
        participant = Volunteers.objects.get(id=pid)
    participant.is_arrived = True 
    participant.save()
    return HttpResponseRedirect("/checkin/")

def check_in(request):
    if request.POST:
        contact = request.POST.get("contact")
        participant = Registration.objects.filter(contact=contact).first()
        ptype = "participant"
        if not participant:
            participant = Volunteers.objects.filter(contact=contact).first()
            ptype= "volunteer"
        return render(request,"checkin.html",{"participant":participant,"ptype":ptype})
    else:
        return render(request,"checkin.html",{})
@require_http_methods(["POST"])
def updatead(request):
    arrival = request.POST.get("arrival")
    departure = request.POST.get("departure")
    id = request.POST.get("id")
    if id and (arrival or departure):
        participant = Registration.objects.get(id=id)
        participant.arrival = arrival
        participant.departure = departure
        participant.save()
        return HttpResponse("Successfully stored information. You can now close this tab")
    else:
        return HttpResponseRedirect("/me_in_camp/")

@require_http_methods(["POST"])
def register(request):
    reg_form = RegistrationForm(request.POST)
    if reg_form.is_valid():
        # Verify if user not alredy registered
        contact = request.POST.get("contact")
        email = request.POST.get("email")
        prev_reg = Registration.objects.filter(contact=contact,email=email)
        if len(prev_reg) > 0:
            return JsonResponse({"header":"Registration already exists","msg":"Registration already exists. If you trying to change your track please email to camp@fsmk.org."},status=201)
        else:
            reg_data = reg_form.save()
            if reg_data.track:
                if reg_data.college.id == settings.HOST_COLLEGE_ID:
                    track_cost_paid = reg_data.track.track_cost - settings.HOST_COLLEGE_CONCESSION
                else:
                    track_cost_paid = reg_data.track.track_cost
            reg_data.track_cost_paid = track_cost_paid
            reg_data.save()
            context = {"name":reg_data.name,"track":reg_data.track.track_name,"fee":track_cost_paid, "tshirt":reg_data.tshirt,"college":reg_data.college.name,"volunteer":reg_data.college.volunteer,"coordinator":reg_data.college.coordinator}
            send_html_email([email],context)
            return JsonResponse({"header":"Registration Successful","msg":"You should have got an email with further details. <span class='text-danger'>Please do check your spam too</span>"},status=200)
    else:
        return JsonResponse({"header":"Error in registration","msg":"There were some errors while registring you. Please do write to camp@fsmk.org to take the registration further."},status=400)


def view_mail(request):
    reg_data = Registration.objects.filter(email="ram.seshan.cs@gmail.com").first()
    cost = 2000
    if reg_data.track == "iot":
        cost = 3800
    context = {"name":reg_data.name,"track":reg_data.get_track_display(),"fee":cost, "tshirt":reg_data.tshirt,"college":reg_data.college.name,"volunteer":reg_data.college.volunteer,"coordinator":reg_data.college.coordinator}
    return render(request, "register_confirm.html",context)


@staff_member_required
def dashboard(request):
    context = {}
    
    # Registration stats 
    context["total_registered"] = Registration.objects.count()
    context["total_confirmed"] = Registration.objects.filter(is_confirmed=True).count()
    context["total_unconfirmed"] = Registration.objects.filter(is_confirmed=False).count()

    # Track Registration stats 
    track_registered = {}
    for track in Track.objects.all():
        track_registered[track.track_name] = []
    print(track_registered)
    for track_count in Registration.objects.filter(is_confirmed=True).order_by('track__track_name').values('track__track_name').annotate(count=Count('track__track_name')):
        track_registered[track_count["track__track_name"]].append(track_count["count"])
    for track_count in Registration.objects.filter(is_confirmed=False).order_by('track__track_name').values('track__track_name').annotate(count=Count('track__track_name')):
        track_registered[track_count["track__track_name"]].append(track_count["count"])
    context["track_registered"] = track_registered

    # Gender Registration stats 
    gender_registered = {"male":[],"female":[],"others":[]}
    for gender_count in Registration.objects.filter(is_confirmed=True).order_by('gender').values('gender').annotate(count=Count('gender')):
        gender_registered[gender_count["gender"]].append(gender_count["count"])
    for gender_count in Registration.objects.filter(is_confirmed=False).order_by('gender').values('gender').annotate(count=Count('gender')):
        gender_registered[gender_count["gender"]].append(gender_count["count"])
    context["gender_registered"] = gender_registered

    # Tshirt size stats
    tshirt_sizes = {"S":[],"M":[],"L":[],"XL":[],"XXL":[]}
    for count in Registration.objects.filter(is_confirmed=True).order_by('tshirt').values('tshirt').annotate(count=Count('tshirt')):
        tshirt_sizes[count["tshirt"]].append(count["count"])
    for count in Registration.objects.filter(is_confirmed=False).order_by('tshirt').values('tshirt').annotate(count=Count('tshirt')):
        tshirt_sizes[count["tshirt"]].append(count["count"])


    context["tshirt_sizes"] = tshirt_sizes
    context["my_colleges"] = Colleges.objects.filter(Q(volunteer=request.user) | Q(coordinator=request.user))
    context["my_participants"] = Registration.objects.filter((Q(college__volunteer=request.user) | Q(college__coordinator=request.user)) & (Q(is_confirmed=False))).order_by("college")

    return render(request, "dashboard.html",context)