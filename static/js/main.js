$(window).on('scroll', function () {
  if(window.outerWidth > 425) {
  if ($(window).scrollTop() < 100) {
      $('.header').removeClass('sticky_header');
  } else {
      $('.header').addClass('sticky_header');
  }
}
else {
  $('.header').addClass('sticky_header'); 
}
});
$(window).on('load', function () {
  $('.header').removeClass('sticky_header');
});
/* ==========================================================================
   WOW Scroll Spy
   ========================================================================== */
   var wow = new WOW({
    //disabled for mobile
	    mobile: false
	});
	wow.init();

/* ==========================================================================
   Back Top Link
   ========================================================================== */

  var offset = 200;
  var duration = 500;
  $(window).scroll(function() {
    if ($(this).scrollTop() > offset) {
      $('.back-to-top').fadeIn(400);
    } else {
      $('.back-to-top').fadeOut(400);
    }
  });
  $('.back-to-top').click(function(event) {
    event.preventDefault();
    $('html, body').animate({
      scrollTop: 0
    }, 600);
    return false;
  })


$(".register").click(function(e){
  // Populate the track name to the modal
  $(".modal-title").text("Registering for " + $("#"+$(this).attr("data-id")).text())
  var selected = $(this).attr("data-track-id")
  $("#id_track").val(selected)
  $('#id_track').prop('readonly', 'readonly');
  // $("#track_description").text($("#"+$(this).attr("data-id")).parent().children()[1].text() + " registration")

  $("#register_modal").modal('show')
})

$(".text-muted").hide()


$('.collapse').collapse({
  toggle:false
})

$("#register").click(function(){
  $(this).attr("disabled","disabled")
  $.ajax({
    url: "/register/",
    type:"POST",
    data: $("#register_form").serialize(),
    success: function(response){
      $("#register_form").hide()
      $("#response_header").text(response.header)
      $("#response_message").html(response.msg)
      $("#register_success").removeClass("invisible").addClass("visible")
    },
    error: function(response){
      $("#error_header").text(response.header)
      $("#error_message").html(response.msg)
    }
  }).fail(function(data){
    var status = data.status;
    var reason = data.reason; 
    console.log(reason)
});
})